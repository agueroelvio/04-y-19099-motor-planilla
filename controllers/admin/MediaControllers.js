
const express = require('express');
const { getALL, db } = require('../../db/conexion'); 
const multer = require('multer');

const MediaController ={

    index:async function (req,res){
        try {
            const mediaItems = await getALL(`SELECT * FROM Media`);
    
            if (mediaItems.length === 0) {
                return res.render("admin/Media/index", { message: "No hay elementos de media", mediaItems: [] });
            }
    
            // Convertir el estado de esta_borrado a "Activo" o "Inactivo"
            const processedMediaItems = mediaItems.map(item => ({
                ...item,
                esta_borrado: item.esta_borrado ? 'Inactivo' : 'Activo'
            }));
    
            res.render("admin/Media/index", { mediaItems: processedMediaItems });
        } catch (error) {
            console.error(error);
            return res.status(500).render("error");
        }
    },
    create: async function (req,res) { 
        const mensaje = req.query.mensaje;
        const datosForm = req.query;
        res.render('admin/Media/crearForm', {
            mensaje: mensaje,
            datosForm: datosForm,
        });
    },
    store: async function (req,res) { 
        try {
            const { matricula, url, titulo, parrafo, video, tipo_medio, esta_borrado } = req.body;
            let imagenPath = '';
            let embedUrl = '';
    
            const matriculaRegex = /^Y\d{5}$/;
            if (!matriculaRegex.test(matricula)) {
                const queryParams = new URLSearchParams(req.body).toString();
                return res.redirect(`/admin/Media/crear?mensaje=La matrícula debe comenzar con 'Y' seguido de 5 dígitos&${queryParams}`);
            }
    
            if (req.file) {
                const nombreArchivo = titulo.replace(/\s+/g, '-').toLowerCase();
                const extension = path.extname(req.file.originalname);
                const nuevoNombreArchivo = `${nombreArchivo}-${Date.now()}${extension}`;
                
                fs.renameSync(req.file.path, path.join(req.file.destination, nuevoNombreArchivo));
                imagenPath = path.join(req.file.destination, nuevoNombreArchivo).replace(/\\/g, '/');
            }
    
            if (video) {
                const videoIdMatch = video.match(/(?:https?:\/\/)?(?:www\.)?(?:youtube\.com\/(?:watch\?v=|embed\/)|youtu\.be\/)([^\s&]+)/);
                if (videoIdMatch) {
                    const videoId = videoIdMatch[1];
                    embedUrl = `https://www.youtube.com/embed/${videoId}`;
                } else {
                    const queryParams = new URLSearchParams(req.body).toString();
                    return res.redirect(`/admin/Media/crear?mensaje=URL de YouTube no válida&${queryParams}`);
                }
            }
    
            if (!matricula || !url || !titulo || !parrafo || !tipo_medio) {
                const queryParams = new URLSearchParams(req.body).toString();
                return res.redirect(`/admin/Media/crear?mensaje=Todos los campos son obligatorios&${queryParams}`);
            }
    
            const maxOrdenResult = await new Promise((resolve, reject) => {
                db.get("SELECT MAX(orden) as maxOrden FROM Media", (err, row) => {
                    if (err) {
                        return reject(err);
                    }
                    resolve(row);
                });
            });
    
            const newOrden = (maxOrdenResult.maxOrden || 0) + 1;
    
            db.run(
                "INSERT INTO Media (matricula, url, titulo, parrafo, imagen, video, tipo_medio, esta_borrado, orden) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",
                [matricula, url, titulo, parrafo, imagenPath, embedUrl, tipo_medio, esta_borrado, newOrden],
                (err) => {
                    if (err) {
                        console.error("Error al insertar en la base de datos:", err);
                        return res.sendStatus(500);
                    }
                    res.redirect("/admin/Media/crear?mensaje=Media creado exitosamente");
                }
            );
        } catch (error) {
            console.error('Error al insertar en la base de datos:', error);
            res.status(500).send('Error al insertar en la base de datos');
        }
    },
    Show(){},
    update(){},
    edit(){},
    //Eliminar registro
    destroy(req,res){
        const idMedia = req.params.idMedia;

        db.run("UPDATE Media SET esta_borrado = 1 WHERE id = ?", [idMedia], function (err) {
            if (err) {
                console.error("Error al marcar el medio como inactivo:", err);
                return res.redirect('/admin/Media/listar?message=Error al eliminar el medio');
            }
    
            console.log(`Medio con ID ${idMedia} marcado como inactivo.`);
            res.redirect('/admin/Media/listar?message=Medio eliminado exitosamente');
        });
}
}


module.exports=MediaController;