
const express = require('express');
const { getALL, db } = require('../../db/conexion'); 
const multer = require('multer');

const TipoMedioCotroller ={

    index:async function (req,res){
    try {
        const tipoMedioItems = await getALL(`SELECT * FROM TipoMedio`);

        if (tipoMedioItems.length === 0) {
            return res.render("admin/TipoMedio/index", { message: "No hay elementos de tipo de medio", tipoMedioItems: [] });
        }

        // Convertir el estado de esta_borrado a "Activo" o "Inactivo"
        const processedItems = tipoMedioItems.map(item => ({
            ...item,
            esta_borrado: item.esta_borrado ? 'Inactivo' : 'Activo'
        }));

        res.render("admin/TipoMedio/index", { tipoMedioItems: processedItems });
        } catch (error) {
            console.error(error);
            return res.status(500).render("error");
        }
    },
    create: async function (req,res) { 
        const mensaje = req.query.mensaje;
        const datosForm = req.query;
        res.render('admin/TipoMedio/crearForm', {
            mensaje: mensaje,
            datosForm: datosForm,
        });
    },
    store: async function (req,res){
        try {
            const { tipo, descripcion, esta_borrado } = req.body;
    
            if (!tipo || !descripcion) {
                const queryParams = new URLSearchParams(req.body).toString();
                return res.redirect(`/admin/TipoMedio/crear?mensaje=Todos los campos son obligatorios&${queryParams}`);
            }
    
            const tipoExistente = await new Promise((resolve, reject) => {
                db.get("SELECT COUNT(*) as count FROM TipoMedio WHERE tipo = ?", [tipo], (err, row) => {
                    if (err) {
                        return reject(err);
                    }
                    resolve(row.count > 0);
                });
            });
    
            if (tipoExistente) {
                const queryParams = new URLSearchParams(req.body).toString();
                return res.redirect(`/admin/TipoMedio/crear?mensaje=El tipo ya existe&${queryParams}`);
            }
    
            const maxOrdenResult = await new Promise((resolve, reject) => {
                db.get("SELECT MAX(orden) as maxOrden FROM TipoMedio", (err, row) => {
                    if (err) {
                        return reject(err);
                    }
                    resolve(row);
                });
            });
            const newOrden = (maxOrdenResult.maxOrden || 0) + 1;
    
            db.run(
                "INSERT INTO TipoMedio (tipo, descripcion, esta_borrado, orden) VALUES (?, ?, ?, ?)",
                [tipo, descripcion, esta_borrado, newOrden],
                (err) => {
                    if (err) {
                        console.error("Error al insertar en la base de datos:", err);
                        return res.sendStatus(500);
                    }
                    res.redirect("/admin/TipoMedio/crear?mensaje=Tipo de Medio creado exitosamente");
                }
            );
        } catch (error) {
            console.error('Error al insertar en la base de datos:', error);
            res.status(500).send('Error al insertar en la base de datos');
        }
    }  ,
    Show(){},
    update(){},
    edit(){},
    //Eliminar registro
    destroy(req,res){

    }

}
 
module.exports=TipoMedioCotroller;