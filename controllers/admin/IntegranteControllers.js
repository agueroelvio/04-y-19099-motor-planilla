/*
index - listado
create - formulario de creación
store - método de guardar en la base de datos
show - formulario de ver un registor
update - método de editar un registro
edit - formulario de edición
destroy - operación de eliminar un registro
*/

const express = require('express');
const { getALL, db } = require('../../db/conexion'); 
const multer = require('multer');
const upload = multer({ dest: '../../public/assets' });
const fileUpload = upload.single('imagen');


const IntegranteController = {
    index: async function (req, res){
        try {
            const integrantes = await getALL(`SELECT * FROM Integrante`);
    
            if (integrantes.length === 0) {
                return res.render("admin/Integrante/index", { message: "No hay integrantes", integrantes: [] });
            }
    
            // Convertir el estado de esta_borrado a "Activo" o "Inactivo"
            const processedIntegrantes = await Promise.all(integrantes.map(async integrante => {
                const datos = await getALL(`SELECT * FROM media WHERE matricula = '${integrante.matricula}'`);
                return {
                    ...integrante,
                    esta_borrado: integrante.esta_borrado ? 'Inactivo' : 'Activo',
                    datos
                };
            }));
    
            res.render("admin/Integrante/index", { integrantes: processedIntegrantes });
        } catch (error) {
            console.error(error);
            return res.status(500).render("error");
        }

    },
    create: async function (req,res) { 
    const mensaje = req.query.mensaje;
    const datosForm = req.query;
    res.render('admin/Integrante/crearForm', {
        mensaje: mensaje,
        datosForm: datosForm,
    });
    },
    store: async function (req,res){
       
        try {
            const { matricula, nombre, apellido, rol, codigo, url, esta_borrado } = req.body;
    
            const normalizedRol = rol ? rol.toLowerCase() : '';
    
            if (normalizedRol && normalizedRol !== 'desarrollador' && normalizedRol !== 'ayudante') {
                const queryParams = new URLSearchParams(req.body).toString();
                return res.redirect(`/admin/Integrante/crear?mensaje=El rol debe ser "Desarrollador" o "Ayudante"&${queryParams}`);
            }
    
            const maxOrdenResult = await new Promise((resolve, reject) => {
                db.get("SELECT MAX(orden) as maxOrden FROM Integrante", (err, row) => {
                    if (err) {
                        return reject(err);
                    }
                    resolve(row);
                });
            });
    
            const newOrden = (maxOrdenResult.maxOrden || 0) + 1;
            db.run(
                "INSERT INTO Integrante (matricula, nombre, apellido, rol, codigo, url, esta_borrado, orden) VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
                [matricula, nombre, apellido, rol, codigo, url, esta_borrado, newOrden],
                (err) => {
                    if (err) {
                        console.error("Error al insertar en la base de datos:", err);
                        return res.sendStatus(500);
                    }
                    res.redirect("/admin/Integrante/crear?mensaje=Integrante creado exitosamente");
                }
            );
        } catch (error) {
            console.error('Error al insertar en la base de datos:', error);
            res.status(500).send('Error al insertar en la base de datos');
        }
    },
    Show(){},
    update(){},
    edit(){},
    //Eliminar registro
    destroy(req,res){
        const { idIntegrante } = req.params;

        db.run("DELETE FROM Integrante WHERE id = ?", [idIntegrante], function (err) {
            if (err) {
                console.error("Error al eliminar el integrante:", err);
                return res.redirect('/admin/Integrante/listar?message=Error al eliminar el integrante');
            }

            console.log(`Integrante con ID ${idIntegrante} eliminado.`);
            return res.redirect('/admin/Integrante/listar?message=Integrante eliminado exitosamente');
        });
    }
}


module.exports=IntegranteController;